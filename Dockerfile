FROM python:3.11

WORKDIR /src

COPY requirements.txt requirements.txt
RUN python -m pip install --no-cache-dir -r requirements.txt

COPY KITMeetingButler.py ./
COPY ButlerAPI.py ./

CMD python -u KITMeetingButler.py --queue-server kafka --redis-server redis
